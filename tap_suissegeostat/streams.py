"""Stream type classes for tap-suissegeostat."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_suissegeostat.client import SuisseGeostatStream

SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")


class StatpopStream(SuisseGeostatStream):
    """Define custom stream."""

    name = "bfs_statpop"
    primary_keys = ["RELI", "ERHJAHR"]
    replication_key = None
    schema_filepath = SCHEMAS_DIR / "bfs-statpop.json"


class StatentStream(SuisseGeostatStream):
    """Define custom stream."""

    name = "bfs_statent"
    dataset = "statent"
    primary_keys = ["RELI", "ERHJAHR", "NOGA_VERSION"]
    replication_key = None
    schema_filepath = SCHEMAS_DIR / "bfs-statent.json"


class GWSStream(SuisseGeostatStream):
    """Define custom stream."""

    name = "bfs_gws"
    dataset = "gws"
    primary_keys = ["RELI", "ERHJAHR"]
    replication_key = None
    schema_filepath = SCHEMAS_DIR / "bfs-gws.json"
