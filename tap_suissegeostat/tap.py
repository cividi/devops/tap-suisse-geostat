"""SuisseGeostat tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th

from tap_suissegeostat.streams import (
    SuisseGeostatStream,
    StatpopStream,
    StatentStream,
    GWSStream,
)

STREAM_TYPES = [
    StatpopStream,
    StatentStream,
    GWSStream,
]


class TapSuisseGeostat(Tap):
    """SuisseGeostat tap class."""

    name = "tap-suissegeostat"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "start_date",
            th.DateType,
            description="Earliest dataset to sync",
        ),
        th.Property(
            "download_base_url",
            th.StringType,
            default="https://dam-api.bfs.admin.ch/hub/api/dam/assets",
            description="The url for the API service",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapSuisseGeostat.cli()
