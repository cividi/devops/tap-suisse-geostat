"""Custom client handling, including SuisseGeostatStream base class."""

from urllib import request
import io
import re
import os
from datetime import date
from zipfile import ZipFile
import csv
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk.streams import Stream


class SuisseGeostatStream(Stream):
    """Stream class for SuisseGeostat streams."""

    dataset = "statpop"

    @property
    def start_year(self) -> int:
        start_date = date.fromisoformat(self.config.get("start_date"))
        return start_date.year

    @property
    def get_asset_paths(self) -> str:
        dir = os.path.dirname(os.path.abspath(__file__))
        with open(f"{dir}/urls.csv") as urls_matrix:
            urls = csv.DictReader(urls_matrix)
            paths = []
            for url in urls:
                if int(url["year"]) >= self.start_year and url[self.dataset]:
                    paths.append({"year": int(url["year"]), "url": url[self.dataset]})
            return paths

    def get_records(self, context: Optional[dict]) -> Iterable[dict]:
        if len(self.get_asset_paths) > 0:
            for asset_path in self.get_asset_paths:
                url = f"{self.config.get('download_base_url')}/{asset_path['url']}"
                year = asset_path["year"]
                req = request.Request(url)
                with request.urlopen(req) as resp:
                    with ZipFile(io.BytesIO(resp.read())) as zipfile:
                        delim = ";"
                        if self.dataset == "statpop":
                            if year < 2020:
                                filename = f"{self.dataset.upper()}{year}.csv"
                                delim = ","
                            else:
                                filename = f"ag-b-00.03-vz{year}{self.dataset}/{self.dataset.upper()}{year}.csv"
                        elif self.dataset == "gws":
                            if year < 2020:
                                delim = ","
                                filename = f"{self.dataset.upper()}{year}_HA.csv"
                            else:
                                filename = f"ag-b-00.03-vz{year}{self.dataset}/{self.dataset.upper()}{year}_HA.csv"
                        elif self.dataset == "statent":
                            if year < 2020:
                                filename = f"ag-b-00.03-22-STATENT{year}/{self.dataset.upper()}_N08_{year}.csv"
                            else:
                                filename = f"ag-b-00.03-22-STATENT{year}/{self.dataset.upper()}_{year}.csv"

                        with zipfile.open(filename, "r") as infile:
                            bfs_reader = csv.DictReader(
                                io.TextIOWrapper(infile), delimiter=delim
                            )
                            for row in bfs_reader:
                                if self.dataset == "statpop":
                                    row = dict(
                                        (re.sub(r"^[B|H]\d{2}", "", key), value)
                                        for (key, value) in row.items()
                                    )
                                    row["ERHJAHR"] = year
                                if self.dataset == "statent":
                                    row["NOGA_VERSION"] = list(row.items())[-1][0][0:3]
                                    row = dict(
                                        (re.sub(r"^B\d{2}", "", key), value)
                                        for (key, value) in row.items()
                                    )
                                yield row
